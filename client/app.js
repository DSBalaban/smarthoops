(function() {
  function getRequest(url, callback) {
    var httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = handleResponse;
    httpRequest.open('GET', url);
    httpRequest.send();

    function handleResponse() {
      if (httpRequest.readyState !== XMLHttpRequest.DONE) return;
      if (httpRequest.status === 200) {
        callback(JSON.parse(httpRequest.responseText));
      } else {
        console.error('Ay, Dios mio, it was not a 200!');
      }
    }
  }

  function setScore(score) {
    var playerOne = document.getElementById('playerOne');
    var playerTwo = document.getElementById('playerTwo');

    playerOne.innerText = score.hasOwnProperty('playerOne') ? score.playerOne : playerOne.innerText;
    playerTwo.innerText = score.hasOwnProperty('playerTwo') ? score.playerTwo : playerTwo.innerText;
  }

  getRequest('/api/score/', setScore);

  var socket = io.connect(window.location.href);
  socket.on('score', setScore);
})();