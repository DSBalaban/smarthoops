var express = require('express');
var path = require('path');

var staticFilesRoot = path.join(__dirname, './client/');
var app = express();

app.use(express.static(staticFilesRoot));
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  next();
});

app.get('/', function (req, res) {
  res.sendFile('index.html', {
    root: staticFilesRoot
  });
});

var port = process.env.PORT || 8000;

var server = app.listen(port, function () {
  console.log('Listening on port ' + port);
});

app.use('/api', require('./routes')(server));
