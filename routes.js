function initRoutes(server) {
  var router = require('express').Router();
  var redis = require('redis');
  var redisClient = redis.createClient(process.env.REDIS_URL || '');

  var io = require('socket.io')(server);

  io.on('connection', function(socket) {
    console.log('connected');
    getCurrentScore(function(score) {
      socket.emit('score', score);
    });
  });

  var PLAYER_ONE = 'villentretenmerth';
  var PLAYER_TWO = 'myrgtabrakke';

  redisClient.on('error', function(err) {
    console.log('Error: ' + err);
  });

  redisClient.set(PLAYER_ONE, 0);
  redisClient.set(PLAYER_TWO, 0);

  function getCurrentScore(callback) {
    var score = {};
    var remaining = 2;

    redisClient.get(PLAYER_ONE, function(err, reply) {
      score.playerOne = parseInt(reply, 10) || 0;

      --remaining;
      attemptToRespond();
    });

    redisClient.get(PLAYER_TWO, function(err, reply) {
      score.playerTwo = parseInt(reply, 10) || 0;

      --remaining;
      attemptToRespond();
    });

    function attemptToRespond() {
      if (!remaining) {
        callback(score);
      }
    }
  }

  router.get('/score', function(req, res) {
    getCurrentScore(function(score) {
      res.json(score);
    });
  });

  router.get('/increase-score/:id', function(req, res) {
    var contestants = [PLAYER_ONE, PLAYER_TWO];

    if (!contestants[req.params.id]) {
      res.json({
        error: 'Invalid ID provided',
        success: false
      });

      return;
    }

    incrementScore(contestants[req.params.id]);

    function incrementScore(id) {
      redisClient.get(id, function(err, reply) {
        if (err) {
          handleError(res, err);
          return;
        }

        var newValue = parseInt(reply, 10) + 1;
        redisClient.set(id, newValue, function(err) {
          if (err) {
            handleError(res, err);
            return;
          }

          var score = {};
          score[req.params.id == 0 ? 'playerOne' : 'playerTwo'] = newValue;

          io.emit('score', score);

          res.json(score);
        });
      });
    }
  });

  router.get('/reset', function(req, res) {
    redisClient.set(PLAYER_ONE, 0);
    redisClient.set(PLAYER_TWO, 0);

    io.emit('reset');

    res.json({
      success: true
    });
  });

  function handleError(res, error) {
    console.log(error);
    res.json({
      success: false,
      error: error
    });
  }

  return router;
}

module.exports = initRoutes;